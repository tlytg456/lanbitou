import React from 'react';
import { connect } from 'dva';
// import { Link } from 'dva/router';
import {Layout, Menu, Icon, Row, Col} from 'antd';
import styles from './IndexPage.less';
import Editor from "./Editor";
// import request from '../utils/request';

const { Header, Sider, Content } = Layout;

const customStyles = {
  logo: {
    height: "32px",
    background: "rgba(255,255,255,.2)",
    margin: "16px",
  }
}


class IndexPage extends React.Component {


  constructor() {
    super();
    this.state = {
      collapsed: false,
    };
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }


  render() {

    const editorHeight = document.body.clientHeight - 208;

    return (
      <Layout>
        <Sider
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
        >
          {/*<div className="logo" style={customStyles.logo}/>*/}
          <h2
            style={{
              color: "white",
              margin: 12,
              fontSize: 24,
              marginLeft: 40,
              fontFamily: "Comic Sans MS"
            }}
          >Lan Bi Tou</h2>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1">
              <Icon type="user" />
              <span>nav 1</span>
            </Menu.Item>
            <Menu.Item key="2">
              <Icon type="video-camera" />
              <span>nav 2</span>
            </Menu.Item>
            <Menu.Item key="3">
              <Icon type="upload" />
              <span>nav 3</span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
            <Icon
              className={styles.trigger}
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
          </Header>
          <Content style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 780 }}>
            <Editor
              height={editorHeight}
            />
          </Content>
        </Layout>
      </Layout>
    );
  }
}

IndexPage.propTypes = {
};

export default connect()(IndexPage);

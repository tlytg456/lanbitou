import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Link } from 'dva/router';
import {Col, Row, Tabs, DatePicker, InputNumber, Select, TimePicker, Card, Form} from 'antd';
import brace from 'brace';
import AceEditor from 'react-ace';
import 'brace/mode/markdown';
import 'brace/theme/twilight';
import styles from './IndexPage.less';

const Option = Select.Option;
const FormItem = Form.Item;

const ReactMarkdown = require('react-markdown');

class Editor extends React.Component {


  constructor() {
    super();
    this.state = {
      userInputContent: "",
      fontSize: 14,
    };
  }

  onChange = (newValue) => {
    console.log(`input ${newValue}`);
    this.setState({
      userInputContent: newValue,
    });
  }

  handleChange = (value) => {
    this.setState({
      fontSize: parseInt(value),
    });
  }


  render() {
    return (
      <div>
        <Form layout="inline" style={{ marginBottom: 16 }}>
          <FormItem
            label="Font Size"
          >
            <Select value={this.state.fontSize} style={{ width: 120 }} onChange={this.handleChange}>
              <Option value="12">12</Option>
              <Option value="14">14</Option>
              <Option value="16">16</Option>
              <Option value="18">18</Option>
            </Select>
          </FormItem>
        </Form>

        <Row gutter={16}>
          <Col span={12}>
            <AceEditor
              mode="markdown"
              theme="twilight"
              fontSize={this.state.fontSize}
              tabSize={2}
              style={{
                width: "100%",
                maxHeight: this.props.height,
                minHeight: this.props.height,
                overflow: "scroll",
              }}
              value={this.state.userInputContent}
              onChange={this.onChange}
            />
          </Col>
          <Col span={12}>
            <Card style={{
              maxHeight: this.props.height,
              minHeight: this.props.height,
              overflow: "scroll",
            }}>
              <ReactMarkdown
                source={this.state.userInputContent}
              />
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

Editor.propTypes = {
  height: PropTypes.number,
};

export default connect()(Editor);
